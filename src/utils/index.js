import Scroll, { scroller } from 'react-scroll';

export const scroll = Scroll.animateScroll;
export const scrollTo = element => {
  scroller.scrollTo(element, {
    offset: -56,
    duration: 500,
    delay: 0,
    smooth: 'easeInOutQuart'
  });
};

export const navigate = (props, scrollTarget = null) => {
  const scrollCallback = () => {
    if (scrollTarget) {
      scrollTo(scrollTarget)
    } else {
      scroll.scrollToTop({ marginTop: 56 });
    }
  };

  if (props.location && props.location.pathname !== '/') {
    const homeRoute = scrollTarget ?
      `/#${scrollTarget}` :
      '/';
    props.history.push(homeRoute);
  } else if (props.location.hash.length > 0) {
    props.history.push('/');
    scrollCallback();
  } else {
    scrollCallback();
  }
}