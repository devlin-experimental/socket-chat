import React from 'react';
import Header from './Header';
import Footer from './Footer';

const LayoutHOC = content =>
  <div className="core-layout">
    <Header />
    <div style={{ paddingTop: 56 }}>
      { content }
    </div>
    <Footer />
  </div>;

export {
  Header,
  Footer
};

export default LayoutHOC
