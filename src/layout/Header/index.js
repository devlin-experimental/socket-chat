import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Typography } from '@material-ui/core';
import { navigate } from '../../utils';
import AppBar from '../../components/AppBar';

const styles = {
  cursor: 'pointer',
  fontFamily: '\'Nanum Gothic\', sans-serif'
};

const Header = props =>
  <AppBar>
    <Typography variant="title" color="inherit" className="noselect" styles={ styles } onClick={ () => navigate(props) }>
      Lunch Chat
    </Typography>
    <div style={ { marginLeft: 'auto' } } >
      <Button color="inherit" onClick={ () => { props.history.push('/chat') } }>Chat Now!</Button>
    </div>
  </AppBar>;

export default withRouter(Header);