import React from 'react';
import Section from '../../components/Section';

const getCurrentYear = () => {
  const today = new Date();
  return today.getFullYear();
}

const Footer = () =>
  <Section
    anchor="footer"
    style={ {
      background: '#F1F1F1',
      color: '#777',
      textAlign: 'center'
    } }
  >
    <span className="reduced-font-sm">
      &copy; Copyright { getCurrentYear() } - All rights reserved
    </span>
  </Section>;

export default Footer;
