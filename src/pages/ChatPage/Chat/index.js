import React from 'react';
import { Button, Card, CardContent, InputBase } from '@material-ui/core';
import Section from '../../../components/Section';
import './index.css';

const chatData = {
  users: [],
  messages: [
    { user: 'Juan', message: 'Hello' },
    { user: 'Georgieo', message: 'Hola, Juan' }
  ]
}

export default class Chat extends React.Component {
  state = {
    currentUser: 'Collier',
    newMessage: '',
    chatData
  }

  constructor(props) {
    super(props);

    this.sendMessage = this.sendMessage.bind(this);
    this.simultateResponse = this.simultateResponse.bind(this);
    this.updateNewMessage = this.updateNewMessage.bind(this);
  }

  updateNewMessage = message =>
    this.setState({ newMessage: message });

  sendMessage = () => {
    const { chatData: { messages }, currentUser, newMessage } = this.state;

    this.setState({
      newMessage: '',
      chatData: {
        messages: [
          ...messages,
          { message: newMessage, user: currentUser }
        ]
      }
    }, () => { this.simultateResponse(); });
  }

  simultateResponse = () => {
    const { chatData: { messages } } = this.state;
    const currentUser = 'BOT Man'
    const newMessage = 'Me do not understand yet.';

    this.setState({
      chatData: {
        messages: [
          ...messages,
          { message: newMessage, user: currentUser }
        ]
      }
    })
  }

  render = () =>
    <Section
      anchor="chat-section"
      background="#EEE"
      color="#222"
      style={ { minHeight: '450px' } }
    >
      {
        this.state.chatData.messages.map(mess =>
          <div className="message">
            { mess.user }
            <Card>
              <CardContent>
                { mess.message }
              </CardContent>
            </Card>
          </div>
        )
      }

      <form onSubmit={ e => { this.sendMessage(); e.preventDefault();  } }>
        <InputBase onChange={ e => this.updateNewMessage(e.target.value) } value={ this.state.newMessage } />
        <Button type="submit">Send</Button>
      </form>
    </Section>;
}
