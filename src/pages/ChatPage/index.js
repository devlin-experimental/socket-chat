import React from 'react';
import { withRouter } from 'react-router-dom';
import Chat from './Chat';

class ChatPage extends React.Component {
  render() {
    return <div className="chat-page">
      <Chat { ...this.props } />
    </div>;
  }
}  

export default withRouter(ChatPage);
