import React from 'react';
import Button from '../../../components/Button';
import Section from '../../../components/Section';
import { navigate } from '../../../utils';

export default props =>
  <Section
    anchor="intro-chat-section"
    background="#999"
    color="white"
  >
    <h2>Lunch Bot wants to Chat!</h2>
    <p>Make Lunches Great Again</p>
    <Button color="primary" className="class" onClick={ () => { props.history.push('/chat') } }>Chat Now!</Button>
  </Section>;