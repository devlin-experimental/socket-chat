import React from 'react';
import Button from '../../../components/Button';
import Section from '../../../components/Section';

export default props =>
  <Section
    anchor="links-section"
    background="#f5f5f5"
    color="#222"
  >
    <p>Check out the following resources for more information.</p>
    <Button color="secondary" className="class">Documentation</Button>
    <Button color="secondary" className="class">View on Gitlab</Button>
  </Section>;