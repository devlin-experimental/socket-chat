import React from 'react';
import { withRouter } from 'react-router-dom';
import Intro from './Intro';
import Links from './Links';
import { scrollTo } from '../../utils';

class HomePage extends React.Component {
  componentDidMount() {
    if (this.props.location && this.props.location.hash) {
      scrollTo(this.props.location.hash.replace('#', ''));
    }
  }

  render() {
    return <div className="home-page">
      <Intro { ...this.props } />
      <Links { ...this.props} />
    </div>;
  }
}  

export default withRouter(HomePage);
