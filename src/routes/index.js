import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import MainPage from '../pages/MainPage';
import ChatPage from '../pages/ChatPage';
import NotFoundPage from '../pages/NotFoundPage';
import buildLayout from '../layout';

const routes = [
  {
    path: '/',
    component: MainPage
  },
  {
    path: '/chat',
    component: ChatPage
  }
]

const AppRouter = () => (
  <Router>
    {
      buildLayout(
        <div>
          <Switch>
            {
              routes.map((props, index) => {
                let actualProps = { ...props };

                if (index === 0) {
                  actualProps = {
                    ...actualProps,
                    exact: true
                  }
                }

                return <Route key={`route-${index}`} { ...actualProps } />;
              })
            }
            <Route component={ NotFoundPage } />
          </Switch>
        </div>
      )
    }
  </Router>
);

export default AppRouter;