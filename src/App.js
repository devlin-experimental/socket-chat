import React, { Component } from 'react';
import { connect } from 'react-redux'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Router from './routes';
import './App.css';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#a5d6a7',
    },
    secondary: {
      main: '#26a69a',
    },
  },
});

class App extends Component {

  render() {
    return <MuiThemeProvider theme={ theme }>
      <Router />
    </MuiThemeProvider>
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
