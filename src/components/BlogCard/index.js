import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  media: {
    height: 250,
    fontFamily: '\'Abel\', sans-serif'
  },
};

const MediaCard = props => {
  const {
    classes,
    title,
    summary,
    imageSrc
  } = props;
  return (
    <Card className={classes.card} style={ { width: '100%' } }>
      <CardActionArea style={ { width: '100%' } }>
        <CardMedia
          className={classes.media}
          image={ imageSrc || '' }
          title={ title }
        />
        <CardContent>
          <Typography gutterBottom variant="headline" component="h2">
            { title }
          </Typography>
          <Typography component="p">
            { summary }
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

MediaCard.propTypes = {
  classes: PropTypes.object.isRequired,
  imageSrc: PropTypes.string
};

export default withStyles(styles)(MediaCard);