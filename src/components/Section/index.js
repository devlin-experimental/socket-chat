import React from 'react';
import './index.css';

const renderTitle = title => title
  ? <h4 className="title">{ title }</h4>
  : null;

const Container = ({
  children,
  stretch
}) => {
  return !stretch
    ? <div className="container">{ children }</div>
    : <div>{ children }</div>
}

const Section = ({
  anchor,
  background,
  backgroundImage,
  backgroundSize,
  children,
  color,
  stretch,
  style,
  title
}) => {
  const inlineStyles = {
    background,
    backgroundImage: backgroundImage && `url(${backgroundImage})`,
    backgroundSize,
    color,
    ...style
  };

  return <section id={ anchor } name={ anchor } className="section-component" style={ inlineStyles }>
    <Container stretch={ stretch }>
      { renderTitle(title) }
      { children }
    </Container> 
  </section>
}

export default Section;