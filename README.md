Run presentation:

```
  $ npm run start:presentation
```

Run development:

```
  $ npm start
```

Run production build:

```
  $ npm run build
```